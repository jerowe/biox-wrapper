# BioX::Wrapper

Base class for BioX::Wrapper

## Wrapper Options

### example.yml

    ---
    indir: "/path/to/files"
    outdir: "path/to/testdir"

### comment\_char

For a bash script a comment is "#", but is other characters for other languages

## print\_opts

Print out the command line options

### indir

A path to your vcf files can be given, and using File::Find::Rule it will recursively search for vcf or vcf.gz

### outdir

Path to write out annotation files. It creates the structure

    outdir
        --annovar_interim
        --annovar_final
        --vcf-annotate_interim #If you choose to reannotate VCF file
        --vcf-annotate_final #If you choose to reannotate VCF file

A lot of interim files are created by annovar, and the only one that really matters unless you debugging a new database is the multianno file found in annovar\_final

If not given the outdirectory is assumed to be the current working directory.

# NAME

BioX::Wrapper - Base class for BioX::Wrappers

# SYNOPSIS

    use BioX::Wrapper;

# DESCRIPTION

BioX::Wrapper is

# Acknowledgements

This module was originally developed at and for Weill Cornell Medical
College in Qatar within ITS Advanced Computing Team. With approval from
WCMC-Q, this information was generalized and put on github, for which
the authors would like to express their gratitude.

# AUTHOR

Jillian Rowe <jillian.e.rowe@gmail.com>

# COPYRIGHT

Copyright 2015 - Weill Cornell Medical College in Qatar

# LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

# SEE ALSO
